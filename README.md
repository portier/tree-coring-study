# Tree coring study

## Description
This repository contains the code used in the analyses performed in
Portier, Shackleton, Klesse et al. No evidence that coring affects tree growth or mortality in three common European temperate forest tree species. Eur J Forest Res 143, 129–139 (2024). https://doi.org/10.1007/s10342-023-01612-6   
This paper assesses the effect of tree coring on growth and mortality of beech, spruce and fir using inventory data from Ukraine and Switzerland. 

## Data
The directory "data" contains the processed data necessary to run the analyses, including the datasets corresponding to the Swiss Vordemwald site as well as the Ukrainian Uholka 10 ha plot and corresponding four satellite plots. The Ukrainian Sample Plot Inventory data are not provided as they are not yet publicly available.
The dataframe contains one row per tree included in the analyses and the following columns:
- diameter_A: diameter at breast height of each tree at the time of the first inventory
- COMPETITIONcor: competition index (corresponding to what is refered to in the paper as CompInd)
- cored: set to TRUE for trees that were cored, and to FALSE for trees that were not cored
- alive: set to TRUE for trees that were alive at both inventories, and to FALSE for trees that were alive at the first inventory and dead at the second inventory
- BAI: Basal Area Increment of each tree
- target: set to "growth" for the data used in the growth analyses, and to "mort" for the data used in the mortality analyses
- data: defines to which dataset a tree belongs to (see paper to further description of the datasets)

## How to run the code
The directory "R" contains the code performing the analyses. First, make sure all libraries loaded by each R-script file are installed. 
- The R-script "Analyses" should be run to perform all the analyses (growth and mortality). It calls the "helping_function" R-script.
- The R-script "helping_function" does not need to be run. It is called automatically by the Analyses R-script. It contains the functions performing the propensity score analyses and the bootstrap procedure.
- The R-script "Figures" contains the code creating the figures of the paper. The R-script "Analyses" should be run first for this one to work. 

## Licence
See Licence file in this repository.  

## Authors
This code was developed by Jeanne Portier and Stefan Klesse.
The authors of the corresponding study are Jeanne Portier, Ross T. Shackleton, Stefan Klesse, Marco Ferretti, Roman Flury, Martina L. Hobi, Jonas Stillhard, Georg von Arx, Brigitte Rohner, Esther Thürig, all affiliated to the [Swiss Federal Institute for Forest Snow and Landscape Research WSL](https://www.wsl.ch/en/index.html).

## Contact
For further questions please contact jeanne.portier@wsl.ch or stefan.klesse@wsl.ch.
